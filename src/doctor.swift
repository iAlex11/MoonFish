//
//  doctor.swift
//  MoonFish
//
//  Created by Alex Modroño Vara on 31/08/2018.
//  Copyright © 2018 Alex Modroño Vara. All rights reserved.
//

import Foundation
import SystemConfiguration

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0)).takeRetainedValue()
        }
        
        var flags: SCNetworkReachabilityFlags = 0
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == 0 {
            return false
        }
        
        let isReachable = (flags & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection) ? true : false
    }
    
}

@discardableResult
func shell(_ args: String...) -> Int32 {
    let task = Process()
    task.launchPath = "/usr/bin/env"
    task.arguments = args
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

func ArgsExist(n: Int) -> Bool { // simple way to check if a determine number of command line arguments exist.
    if n <= CommandLine.arguments.count {
        return true
    } else {
        return false
    }
}

func checkArgs(arg: String) -> Bool { // checks if a specific argument was passed.
    if CommandLine.arguments[1] == arg {
        return true
    } else {
        return false
    }
}

if ArgsExist(n: 2) {
    if checkArgs(arg: "connection") {
        if Reachability.isConnectedToNetwork() { //Checks for internet connection.
            //Has internet.
            print("Doctor wasn't able to detect any problems on your internet connection.")
        } else {
            print("Error: Could not resolve any internet connection. This might be the main problem.")
        }
    }
}
