#
# This simple file is the cURL and wGet command for MoonFish
# MoonFish install package in Python 3.7
#
# Simple; v0.1.0
# More info at https://moonfish.com/packages/standard/install_tool
#

import os
import sys
import emoji
import time
from halo import Halo

website = "[FTP_URL GOES HERE]"

class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def main(Package):
    try:
        if sys.argv[2] == "package":
            text1 = color.YELLOW + "Looking for package recipes with name " + Package + "..." + color.END
            spinner = Halo(text=text1, spinner='dots')
            spinner.start()
            os.system("curl -k -s " + website + "/" + Package + "/ -o /Library/MoonFish/Packages/" + Package)
            spinner.stop()
            print(emoji.emojize(":package: Done!"))
    except IndexError:
        if sys.argv[1] == "install":
            text2 = color.YELLOW + "Looking for recipes with name " + Package + "..." + color.END
            spinner = Halo(text=text2, spinner='dots2')
            spinner.start()
            os.system("curl -k -s " + website + "/" + Package + "/ -o /Library/MoonFish/Installed/" + Package)
            spinner.stop()
            print(emoji.emojize(":package: Done!"))
        else:
            print("error")

if sys.argv[1] == "credits":
    os.system("python3 -c \"$(curl -s -k https://gist.githubusercontent.com/iAlex11/e9ab83a10f9186e6455d32f01554735b/raw/b18c6955dd26fc5900de6da601099e05eb94403f/credits.py)\"") #run credits gist.
else:
    main(sys.argv[1])
quit()
