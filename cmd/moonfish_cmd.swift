//
//  moonfish_cdm.swift
//  WPL
//
//  Created by Alex Modroño Vara on 01/05/2019.
//  Copyright © 2019 Alex Modroño Vara. All rights reserved.
//

import Foundation

func ArgsExist(n: Int) -> Bool { // simple way to check if a determine number of command line arguments exist.
    if n <= CommandLine.arguments.count {
        return true
    } else {
        return false
    }
}

func checkArgs(arg: String) -> Bool { // checks if a specific argument was passed.
    if CommandLine.arguments[1] == arg {
        return true
    } else {
        return false
    }
}

func HelpPage() {
    print("\u{001B}[0;32mHelp Guide:\u{001B}[0;0m")
    print("\u{001B}[0;34m    credits:\u{001B}[0;0m display moonfish' credits.")
    print("\u{001B}[0;34m    install:\u{001B}[0;0m install a program using a recipe.")
    print("\u{001B}[0;34m    packages:\u{001B}[0;0m \u{001B}[0;36minstall:\u{001B}[0;0m install a package using a recipe.")
    print("             \u{001B}[0;36mmanage:\u{001B}[0;0m manage packages.")
    print("\u{001B}[0;34m    cget:\u{001B}[0;0m install a program using a cobj.")
    print("\u{001B}[0;34m    -n:\u{001B}[0;0m recipe: create a new recipe using the command line.")
    print("\u{001B}[0;34m    --help:\u{001B}[0;0m help page.")
    print("\u{001B}[0;34m    --config:\u{001B}[0;0m configuration.")
}

@discardableResult
func shell(_ args: String...) -> Int32 {
    let task = Process()
    task.launchPath = "/usr/bin/env"
    task.arguments = args
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

@discardableResult
func pip3(_ args: String...) -> Int32 {
    let task = Process()
    task.launchPath = "/usr/local/bin/"
    task.arguments = args
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

@discardableResult
func bin1(_ args: String...) -> Int32 {
    let task = Process()
    task.launchPath = "/usr/bin"
    task.arguments = args
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

func mkdir(folderName: String) {
    let fileManager = FileManager.default
    if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
        let filePath =  tDocumentDirectory.appendingPathComponent("\(folderName)")
        if !fileManager.fileExists(atPath: filePath.path) {
            do {
                try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("\u{001B}[0;31mAn error ocurred while setting up enviroment...\u{001B}[0;0m")
            }
        }
    }
}

func mv(file: String, fnPath: String) {
    // Create a FileManager instance
    
    let fileManager = FileManager.default
    
    // Move 'hello.swift' to 'subfolder/hello.swift'
    
    do {
        try fileManager.moveItem(atPath: file, toPath: fnPath)
    }
    catch let error as NSError {
        print("Ooops! Something went wrong: \(error)")
    }
}

func rmrf(path: String) {
    let fileManager = FileManager.default
    
    // Delete 'hello.swift' file
    
    do {
        try fileManager.removeItem(atPath: path)
    }
    catch let error as NSError {
        print("\u{001B}[0;31mOoops! Something went wrong: \(error)\u{001B}[0;0m")
    }
}

if ArgsExist(n: 2) { //an argument was entered to the command.
    if checkArgs(arg: "--help") { //help page
        HelpPage()
    } else if checkArgs(arg: "credits") {
        shell("python3", "/Library/MoonFish/Packages/install_tool.py", "credits")
    } else if checkArgs(arg: "new") {
        if ArgsExist(n: 3) { //a name for the new instance was entered
            print("\u{001B}[0;32mPreparing enviroment...\u{001B}[0;0m")
            mkdir(folderName: CommandLine.arguments[2])
            mkdir(folderName: CommandLine.arguments[2] + "/src")
            mkdir(folderName: CommandLine.arguments[2] + "/moonfish")
            print("\u{001B}[0;32mDownloading and Installing files...\u{001B}[0;0m")
            shell("/git clone https://github.com/iAlex11/MoonFish.git " + CommandLine.arguments[2] + "/moonfish")
            rmrf(path: CommandLine.arguments[2] + "/moonfish/cmd") //we dont want to re-download the commands
            print("\u{001B}[0;32mInstalling Python dependencies...\u{001B}[0;0m")
            pip3("pip3 install " + CommandLine.arguments[2] + "/dependencies.txt") //install the dependencies
            print("\u{001B}[0;32mMoving libraries...\u{001B}[0;0m")
            mkdir(folderName: CommandLine.arguments[2] + "/src/Dylibs")
            mv(file: CommandLine.arguments[2] + "/moonfish/lib/Moonfish-OSXLib/dylib/Moonfish-OSXLib.framework ", fnPath: CommandLine.arguments[2] + "/src/Dylibs/")
            mv(file: CommandLine.arguments[2] + "/moonfish/lib/Moonfish-iOSLib/dylib/Moonfish-OSXLib.framework", fnPath: CommandLine.arguments[2] + "/src/Dylibs/")
            print("\u{001B}[0;32mCreating source files...\u{001B}[0;0m")
            bin1("curl -s -k https://gist.github.com/iAlex11/8eee3e9e0a3e0fe0aa641abf2d8f601b -o " + CommandLine.arguments[2] +  "/src/InstanceDelegate.swift")
            bin1("curl -s -k -o " + CommandLine.arguments[2] + "/src/InstanceStarted.swift")
        } else {
            print("\u{001B}[0;31mPlease enter a name for your instance.\u{001B}[0;0m")
            print("Lacking of ideas? Try tony_stark or thanos999 ;)")
        }
    } else {
        print("\u{001B}[0;31mError: No such command known as \"" + CommandLine.arguments[1] + "\" exists. \u{001B}[0;0m")
        //HelpPage()
    }
}
