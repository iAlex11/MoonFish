<img align="left" src="https://ialex11.github.io/assets/moonfishIcon-Round.png" width="150" height="150"></img>

# Moonfish | The Library for creating package managers is finally here.
[![Website](https://img.shields.io/badge/-website-9cf.svg)](https://ialex11.github.io/tools/moonfish)
[![version](https://img.shields.io/badge/version-1.0-informational.svg)](https://ialex11.github.io/tools/moonfish#changelog)
[![phase](https://img.shields.io/badge/phase-BETA-orange.svg)](https://ialex11.github.io/tools/moonfish#phase)

Moonfish is an open-source, fast, free and easy to use library for creating your own package managers. It serves as a fast and reliable way to install files from your FTP to a device.

## Getting Started
*Ripped from [iAlex11 Tools / Moonfish](https://ialex11.github.io/tools/moonfish)*

Installing moonfish is really straight-forward proccess. Moonfish is made in `Python` and `Swift`, so you'll
need to install them. Take note that in order for it to work on Windows you'll need to install `Swift for Windows`.

### Python and Swift installation
#### Python installation

<kbd>$ brew install python3 </kbd>

*Note: take note that depending on what OS you are using the command may vary from *py* to *python* to *python3**.

Now to check if Python was installed correctly, run:

<kbd>[python/python3] -V</kbd>

If it returns the python version, you are done with python! :)

#### Swift installation

##### macOS X
If you are on macOS you just need to install Xcode from the *Mac App Store* and it will automatically install Swift.

##### Windows
There's no official way for moonfish to be ran on Windows. In fact, you'll need to change the source code for it to work.
If you really want to run moonfish on windows try downloading someone's fork with the necessary changes.

Moonfish v1.1 will come with Windows support. You can check it's development process [here](https://ialex11.github.io/development/moonfish/v1.1).

For `Swift` to run on windows, just download `Swift for Windows` compiler from the [official website](https://swiftforwindows.github.io/).

##### Linux
If you are on any Linux distro, download **Swift files** from the [Swift's Official Website](https://swift.org/download/) and install them manually, or just [follow this tutorial](https://developerinsider.co/install-apple-swift-on-windows-10-subsystem-for-linux-wsl/).

To check if Swift was installed correctly, run:

<kbd>swift -version</kbd>

If it returns the Swift version, you'll be done with the installation!

### Setting up Moonfish
Download the latest release from the [Moonfish Github Repo](https://github.com/iAlex11/MoonFish/releases). Once downloaded, go to terminal and go to the downloaded moonfish folder using <kbd>cd</kbd>.
Once there, run:

<kbd>python setup.py</kbd>

*Note: if you also have python 2.7.x on your computer you might have to use `python3` instead of `python` command.*

`setup.py` will automatically install all the necessary files for moonfish to work (src, lib, cmd) and install all the commands. `setup.py` shows every step and can be stopped at any time. 

You'll be required to enter a path for moonfish to be installed. By default, moonfish installation path is `/Library/Moonfish`, so if you just want the default path, just leave it in blank and press enter, this will tell `setup.py` you want the default path. Take note that if you install another path than the default one, you'll need to add it to `$PATH` for the `moonfish` command to work. [See this](#adding-a-external-path-to-$path)

You'll also be required to enter python, swift and swiftc installation paths. To know which are them, just open another terminal window WITHOUT closing the one running `setup.py` and run the following command:

**To obtain Python 3.x path**:

<kbd> which python </kbd>

*Note: this will return you the python path. If you also have python 2.7.x, this will return you the python2 path. As moonfish requires the Python3.x version, just replace python with python3 (i.e. <kbd> which python3 </kbd>)*

**To obtain Swift paths (swift and swiftc)**:

<kbd> which swift swiftc </kbd>

*Note: this will return two paths: the first one is the swift's path, the second one is the swiftc's path. So when you are asked for the swift path, enter the first one. When you are asked for the swiftc path, enter the second one.*

#### Adding a external path to $PATH
For the `moonfish` command to work you'll need to add the moonfish installation path to `$PATH`. For that, you'll need to modify either `.bash_profile` or `.zshrc` depending on your shell.

##### Bash shell:
To add `moonfish` command path to `$PATH` in **bash shell** follow the next steps:
1. Find `.bash_profile`. It usually is in your main folder (`~`). To go to there enter <kbd>cd ~</kbd>.
2. Open `.bash_profile` with the editor of your preference.
3. Add the following line: 
<kbd>export PATH="$PATH:[MOONFISH_PATH]"</kbd>
*Where [MOONFISH_PATH] enter the path you chose for moonfish installation*
4. Save `.bash_profile` and run <kbd>source .bash_profile</kbd> to load the changes.
5. You're done!

##### Zsh shell:
The zsh shell process is the same as the bash shell process, just that the file name changes.
To add `moonfish` command path to `$PATH` in **zsh shell** follow the next steps:
1. Find `.zshrc`. It usually is in your main folder (`~`). To go to there enter <kbd>cd ~</kbd>.
2. Open `.zshrc` with the editor of your preference.
3. Add the following line: 
<kbd>export PATH="$PATH:[MOONFISH_PATH]"</kbd>
*Where [MOONFISH_PATH] enter the path you chose for moonfish installation*
4. Save `.zshrc` and run <kbd>source .zshrc</kbd> to load the changes.
5. You're done!

## Contributing

### Local setup

1. `git clone https://github.com/iAlex11/MoonFish.git`
2. `python3 cmd/contribution/contribution_setup.py`  setups moonfish enviroment to program.
3. `python3 cmd/contribution/compile_files.py` once done all the changes, run to compile all moonfish files
4. `python3 cmd/contribution/test_changes.py` runs ./tests/Contribution/InstanceDelegate.swift to test changes

**NOTE**: `contribution_setup.py` creates a folder called `/contribution` where you'll find moonfish API files like if you were creating a new moofish instance, instead is only for testing purposes.

### Submitting changes

Once you've done all the changes you want and tested them, commit the changes and open a `pull request` on GitHub.
Your changes will then automatically be added to the next nightly build of moonfish, and available to download on [releases](https://github.com/iAlex11/moonfish/releases).


<div align="right"><sup>
Coded with ❤️ by <a href="https://twitter.com/Semiak_">@Semiak</a>
</sup></div>
