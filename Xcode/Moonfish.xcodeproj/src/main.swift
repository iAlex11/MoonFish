//
//  main.swift -> MoonFish main swift file (Powered by WPL).
//
//
//  Created by Alex Modroño Vara on 27/7/18.
//
//
//  MoonFish is the world's largest Python3.6 and Swift Package Index and Manager
//  that is FREE, open-source and available for macOS, Linux, Windows and SwiftyOS.
//  It is also a recopilation of terminal tools for downloading and installing files.
//
//  MoonFish installs files by its FTP server, using cURL and wget, using WPL HTTPs Requests, searching for
//  the requested file in GitHub and GitLab (GitTaps), git clone (also GitTaps), by accessing external FTP Servers (EFS) and more (see https://moonfish.org/docs/hiw)
//
//  Even thought MoonFish is thought for Python and Swift programming languages, it can be extended with a large
//  list of plugins that allow you to use MoonFish for everything you want, from installing applications through
//  installing packages for internet browsers (IBP) to installing packages, textures, music, application resources, etc in your computer.
//
//  Thanks to WPL, MoonFish is multiplatform, works perfectly with internet browsers (IE10, Google Chrome, Chromium, Safari and Mozilla Firefox), can be extended and is able to interpret recipes (Ender).
//
//
// Colors:
// \u{001B}[\(attribute code like bold, dim, normal);\(color code)m
//
// Color codes
// black   30
// red     31
// green   32
// yellow  33
// blue    34
// magenta 35
// cyan    36
// white   37

import Foundation

var BrowserType: String
var program = (version: 1.0, name: "MoonFish", vt: "MFS", vf: "HomeGet")

func ArgsExist(n: Int) -> Bool { // simple way to check if a determine number of command line arguments exist.
    if n <= CommandLine.arguments.count {
        return true
    } else {
        return false
    }
}

func checkArgs(arg: String) -> Bool { // checks if a specific argument was passed.
    if CommandLine.arguments[1] == arg {
        return true
    } else {
        return false
    }
}

func HelpPage() {
    print("\u{001B}[0;32mHelp Guide:\u{001B}[0;0m")
    print("\u{001B}[0;34m    credits:\u{001B}[0;0m display moonfish' credits.")
    print("\u{001B}[0;34m    install:\u{001B}[0;0m install a program using a recipe.")
    print("\u{001B}[0;34m    packages:\u{001B}[0;0m \u{001B}[0;36minstall:\u{001B}[0;0m install a package using a recipe.")
    print("             \u{001B}[0;36mmanage:\u{001B}[0;0m manage packages.")
    print("\u{001B}[0;34m    cget:\u{001B}[0;0m install a program using a cobj.")
    print("\u{001B}[0;34m    -n:\u{001B}[0;0m recipe: create a new recipe using the command line.")
    print("\u{001B}[0;34m    --help:\u{001B}[0;0m help page.")
    print("\u{001B}[0;34m    --config:\u{001B}[0;0m configuration.")
}

@discardableResult
func shell(_ args: String...) -> Int32 {
    let task = Process()
    task.launchPath = "/usr/bin/env"
    task.arguments = args
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

if ArgsExist(n: 2) { // checks if commands were entered
    if checkArgs(arg: "--help") { //help page
        HelpPage()
    } else if checkArgs(arg: "credits") {
        shell("python3", "/Library/MoonFish/Packages/install_tool.py", "credits")
    } else if checkArgs(arg: "install") { //install
            if ArgsExist(n: 3){
                let Package = CommandLine.arguments[2] //Package user wants to install
                shell("python3", "/Library/MoonFish/Packages/install_tool.py", Package)
                //shell("python3", "-c", "\"$(curl https://raw.githubusercontent.com/iAlex11/MoonFish/master/Packages/install_tool.py)\"") //executes install_tool.py package/component.

            } else {
                print("\u{001B}[0;31mUnresolved exception: no recipe entered.\u{001B}[0;0m")
                HelpPage()
            }
        }
    else if checkArgs(arg: "packages") {
        if ArgsExist(n: 3) {
            if CommandLine.arguments[2] == "install" {
                if ArgsExist(n: 4) {
                    let Package2 = CommandLine.arguments[3]
                    shell("python3", "/Library/MoonFish/Packages/install_tool.py", Package2, "package")
                } else {
                    print("\u{001B}[0;31mError: No recipe entered.\u{001B}[0;0m")
                }
            } else {
                print(CommandLine.arguments)
                print("\u{001B}[0;31mError: No such command known as \"" + CommandLine.arguments[2] + "\" exists. \u{001B}[0;0m")
            }
        } else {
            print("\u{001B}[1;32mMoonFish Packages:\u{001B}[0;0m")
            shell("ls", "/Library/MoonFish/Packages")
            //print("\u{001B}[1;32mTo change something use:\u{001B}[1;0mmoonfish package delete")
        }
    } else {
        print("\u{001B}[0;31mError: No such command known as \"" + CommandLine.arguments[1] + "\" exists. \u{001B}[0;0m")
        HelpPage()
    }
} else { // No commands were entered.
    print("\u{001B}[0;31mUnresolved exception: no commands entered.\u{001B}[0;0m")
    print("Welcome to MoonFish Package Manager [v", program.version, "] on darwin.", separator: "")
    print("Use '--help' to show guide and '-n' to enter browser modifications.")
    HelpPage()
}
