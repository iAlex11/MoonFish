import os
import sys
import time
from halo import Halo
import ctypes

try: #detect if has sudo priviliges
 is_admin = os.getuid() == 0
except AttributeError:
 is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

if is_admin:
    os.system("clear")
    print(color.GREEN + "Thank you for downloading Moonfish." + color.END)
    print("\nDefault moonfish path is /Library/Moonfish.")
    moonfish_path = input("Moonfish path installation (Leave blank if want default): \033[93m")
    if moonfish_path.upper() == "" or moonfish_path.upper() == "None":
        moonfish_path = "/Library/Moonfish"
    print(color.END + "\n")

    srcInst = Halo(text='Installing moonfish source...', spinner='dots')
    srcInst.start()
    dirname, filename = os.path.split(os.path.abspath(__file__))
    os.system("git clone --quiet https://github.com/iAlex11/moonfish.git " + moonfish_path)
    os.system("mv " + moonfish_path + "/cmd/moonfish_cmd " + moonfish_path + "moonfish")
    srcInst.succeed("Moonfish installed succesfully")
else:
    os.system("clear")
    err = Halo(text="", spinner="dots")
    err.start()
    err.fail("Error: no permissions given.")
    print(color.BLUE + "Please note that Moonfish instalation requires sudo priviliges to move and install files." + color.END)
    print("Please run \"sudo python3 installation\" to install moonfish.")
